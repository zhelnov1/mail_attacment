import imaplib
import email


class MailBox:
    SMTP_SERVER = 'outlook.office365.com'
    SMTP_PORT = 993
    USER = 'maksim.zhelnov@sperasoft.com'
    PASSWORD = 'Mz06121986!!'

    def __init__(self):
        self.imap = imaplib.IMAP4_SSL(host=self.SMTP_SERVER, port=self.SMTP_PORT)
        self.imap.login(self.USER, self.PASSWORD)

    def __enter__(self):
        self.emails = self._get_all_messages()

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.imap.close()
        self.imap.logout()

    def fetch_message(self, num=-1):
        _, data = self.imap.fetch(self.emails[num], '(RFC822)')
        _, bytes_data = data[0]
        email_message = email.message_from_bytes(bytes_data)
        return email_message

    def get_attachment(self, num=-1):
        for part in self.fetch_message(num).walk():
            if part.get_content_maintype() == 'multipart' or part.get('Content-Disposition') is None:
                continue
            if part.get_filename():
                return part.get_payload(decode=True).decode('utf-8').strip()

    def _get_all_messages(self):
        self.imap.select('Inbox')
        status, data = self.imap.search(None, 'ALL')
        return data[0].split()  